# Hidden text in PDFs

## Description

This LaTeX document demonstrates, that by clipping the PDFs in includegraphics, the full graphic is still available in the PDF.
This can especially be bad if you want to remove sensitive data from the included PDF.

![thumbnail](thumbnail.png)

## Link to Overleaf

- [Overleaf](https://www.overleaf.com/read/zynbtndjjgxk)

## License

This document is published under CC-BY-SA 4.0.
