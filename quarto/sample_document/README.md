# Quarto sample document

This document should contain all important features used in scientific writing (at FAST).

## Prerequisites

You need to have Quarto installed (see the [Quarto homepage](https://quarto.org/docs/get-started/)).

For the Python code to execute, you need to have Python3 as well as the following packages installed:

- jupyter
- pandas
- plotly
- statsmodels

If you don't need Python in your documents, you could also remove all Python code from the document.

## Usage

You can convert this document with the following command:

```
quarto render sample_document.qmd
```

You can also choose a different output format:

```
quarto render sample_document.qmd --to html
quarto render sample_document.qmd --to pdf
quarto render sample_document.qmd --to revealjs
```

Please note that the conversion to PDF could require a local LaTeX installation.

## Status of this document

This document is currently WORK IN PROGRESS.

If you find errors, just correct them or mail them to me.

If you find additional examples, just insert them or mail them to me.

## License

You can use this sample document under the CC-BY 4.0 license.
This does not apply to the picture(s) in this folder, they are protected by copyright from KIT.

